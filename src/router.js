import Vue from 'vue'
import VueRouter from 'vue-router'
import auth from 'auth'
import { LocalStorage } from 'quasar'

Vue.use(VueRouter)

function load (component) {
  return () => import(`@/${component}.vue`)
}

export default new VueRouter({
  /*
   * NOTE! VueRouter "history" mode DOESN'T works for Cordova builds,
   * it is only to be used only for websites.
   *
   * If you decide to go with "history" mode, please also open /config/index.js
   * and set "build.publicPath" to something other than an empty string.
   * Example: '/' instead of current ''
   *
   * If switching back to default "hash" mode, don't forget to set the
   * build publicPath back to '' so Cordova builds work again.
   */

  routes: [
    { path: '/', component: load('welcome/welcome'), beforeEnter: checkAuth }, // Default
    { path: '/login', component: load('auth/login') },
    { path: '/register', component: load('auth/register') },
    {
      path: '/',
      component: load('layouts/menu'),
      beforeEnter: checkAuth,
      children: [
        { path: 'profile', component: load('profile/profile'), meta: { title: 'Profile' } },
          { path: 'fitness/all', name:'all-workouts', component:load('fitness/all'),meta:{ title:'All Workouts' } },
          { path: 'fitness/new', component:load('fitness/new'),meta:{ title:'Add new Workout' } },
          { path: 'fitness/view/:id', name:'view-workout', component:load('fitness/show'),meta:{ title:"View workout" } },
          { path: 'fitness/edit/:id', name:'edit-workout', component:load('fitness/edit'),meta:{ title:"Edit workout" } },

          { path: 'income/all', name:'all-incomes', component:load('income/all')},
          { path: 'income/new', name:'new-income', component:load('income/new') },
          { path: 'income/view/:id', name:'view-income', component:load('income/show') },
          { path: 'income/edit/:id', name:'edit-income', component:load('income/update') },

          { path: 'expense/all', name:'all-expenses', component:load('expense/all')},
          { path: 'expense/new', name:'new-expense', component:load('expense/new') },
          { path: 'expense/view/:id', name:'view-expense', component:load('expense/show') },
          { path: 'expense/edit/:id', name:'edit-expense', component:load('expense/update') }
      ]
    },
    { path: '*', component: load('error404') } // Not found
  ]
})

function checkAuth (to, from, next) {
  if (to.path === '/' && auth.user.authenticated) {
    next('/profile')
  }
  else if (!LocalStorage.get.item('id_token') && to.path !== '/') {
    console.log('not logged')
    next('/login')
  }
  else {
    next()
  }
}
