# Petyos Personal Manager

## Getting Started

Clone the repository and install the dependencies:  
`npm install`

Then start a development server:  
`quasar dev`

![Index](https://i.imgur.com/WXpdy7O.png)
![Login](https://i.imgur.com/pkUAYQM.png)
